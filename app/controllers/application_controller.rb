class ApplicationController < Sinatra::Base
  
  enable :sessions
  
  configure do
    set :public_folder, 'public'
    set :views, 'app/views'
  end
  
  get "/" do
    @remaining = 12
    erb :'index.html'
  end
  
  post "/remove" do
    @remaining = params[:remaining].to_i - params[:count].to_i
    erb :'index.html'
  end
  
end
